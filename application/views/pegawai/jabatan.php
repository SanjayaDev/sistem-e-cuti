<div class="content-wrapper">  
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Jabatan</h1>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="card">
      <div class="card-body">
        <a href="" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#add">Tambah Jabatan</a>
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="data">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama Divisi</td>
                <td>Opsi</td>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach($jabatan as $j) { ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?= $j->jabatan_nama; ?></td>
                <td>
                  <a href="<?= base_url('pegawai/hapusJabatan/'.$j->jabatan_id); ?>" class="btn btn-danger btn-sm">Hapus</a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="add">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Tambah Jabatan</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('pegawai/jabatan'); ?>" method="post">
          <div class="form-group">
            <label>Jabatan</label>
            <input type="text" name="jabatan" class="form-control">
          </div>
          <input type="submit" value="Simpan" class="btn btn-success btn-sm">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>