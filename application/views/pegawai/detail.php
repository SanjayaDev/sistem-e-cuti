<div class="content-wrapper">  
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Detail Data Cuti</h1>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="card">
      <div class="card-body">
        <h4 class="text-center mb-5">Data Karyawan</h4>
        <div class="row mb-5">
          <div class="col-md-4 mx-auto">
            <img src="<?= base_url('img/'.$usr->user_nik."/".$usr->user_img); ?>" class="img-cuti">
          </div>
          <div class="col-md-8">
            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                  <th>NIP Karyawan</th>
                  <td>: <?= $usr->user_nik; ?></td>
                </tr>
                <tr>
                  <th>Nama Karyawan</th>
                  <td>: <?= $usr->user_nama; ?></td>
                </tr>
                <tr>
                  <th>Jenis Kelamin Karyawan</th>
                  <td>: <?= $usr->user_jk; ?></td>
                </tr>
                <tr>
                  <th>Tanggal Lahir Karyawan</th>
                  <td>: <?= date('d M Y', strtotime($usr->user_tglLahir)); ?></td>
                </tr>
                <tr>
                  <th>Divisi</th>
                  <td>: <?= $usr->user_divisi; ?></td>
                </tr>
                <tr>
                  <th>Jabatan</th>
                  <td>: <?= $usr->user_jabatan; ?></td>
                </tr>
                <tr>
                  <th>Sisa Cuti</th>
                  <td>: <?= $sisa->sisa_pemakaian; ?> Hari</td>
                </tr>
                <tr>
                  <th>Tanggal Pembuatan Akun</th>
                  <td>: <?= date('d F Y', $usr->user_dateCreated) ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>