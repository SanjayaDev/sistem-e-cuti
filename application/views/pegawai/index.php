<?= $this->session->flashdata('pesan'); ?>
<div class="content-wrapper">  
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Pegawai</h1>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="card">
      <div class="card-body">
        <?php
          foreach($divisi as $d) {
            if($user['user_divisi'] == "Departmen Sumber Daya Manusia" && $user['user_role'] == 2 && $d->divisi_nama == $user['user_divisi']){
        ?>
        <a href="" data-toggle="modal" data-target="#add" class="btn btn-primary btn-sm mb-3">Tambah Data</a>
        <div class="table-responsive mb-5">
          <table class="table table-bordered table-hover d-block mx-auto" id="data">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama Pegawai</td>
                <td>NIP</td>
                <td>Email pegawai</td>
                <td>Divisi</td>
                <td>Jabatan</td>
                <td>Tanggal Masuk Perusahaan</td>
                <td>Tanggal Buat Akun</td>
                <td>Opsi</td>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; 
                $sql = $this->m_cuti->getData('tb_user')->result();
                foreach($sql as $u) {
              ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?= $u->user_nama; ?></td>
                <td><?= $u->user_nik; ?></td>
                <td><?= $u->user_email; ?></td>
                <td><?= $u->user_divisi; ?></td>
                <td><?= $u->user_jabatan; ?></td>
                <td><?= date('d M Y', strtotime($u->user_tglMasukPerusahaan)); ?></td>
                <td><?= date('d F Y', $u->user_dateCreated); ?></td>
                <td>
                  <a href="<?= base_url('pegawai/detailPegawai/'.$u->user_id); ?>" class="btn btn-info btn-sm mb-2">Detail</a>
                  <a href="<?= base_url('pegawai/editPegawai/'.$u->user_id); ?>" class="btn btn-primary btn-sm mb-2">Edit</a>
                  <a href="<?= base_url('pegawai/hapusPegawai/'.$u->user_id); ?>" class="btn btn-danger btn-sm">Hapus</a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>

        <h4 class="mt-5 mb-3">Nomor Refrensi</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <tr>
              <th>No</th>
              <th>Nomor Refrensi</th>
              <th>Role</th>
            </tr>
            <?php $no = 1; foreach($noRef as $n) { ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= $n->no_ref; ?></td>
              <td><?php
                if($n->no_role == 1) {
                  echo '<div class="badge badge-info">Kepala Bagian</div>';
                } elseif($n->no_role == 2) {
                  echo '<div class="badge badge-info">SDM</div>';
                } elseif($n->no_role == 3) {
                  echo '<div class="badge badge-info">Pegawai</div>';
                }
              ?></td>
            </tr>
            <?php } ?>
          </table>
        </div>
        <?php } elseif($user['user_divisi'] == 'Departmen Sumber Daya Manusia' && $user['user_role'] == 1 && $d->divisi_nama == $user['user_divisi']) { ?>
          <a href="" data-toggle="modal" data-target="#add" class="btn btn-primary btn-sm mb-3">Tambah Data</a>
        <div class="table-responsive mb-5">
          <table class="table table-bordered table-hover d-block mx-auto" id="data">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama Pegawai</td>
                <td>Nomor NIK</td>
                <td>Email pegawai</td>
                <td>Divisi</td>
                <td>Jabatan</td>
                <td>Tanggal Masuk Perusahaan</td>
                <td>Tanggal Buat Akun</td>
                <td>Opsi</td>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; 
                $sql = $this->m_cuti->getData('tb_user')->result();
                foreach($sql as $u) {
              ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?= $u->user_nama; ?></td>
                <td><?= $u->user_nik; ?></td>
                <td><?= $u->user_email; ?></td>
                <td><?= $u->user_divisi; ?></td>
                <td><?= $u->user_jabatan; ?></td>
                <td><?= date('d M Y', strtotime($u->user_tglMasukPerusahaan)); ?></td>
                <td><?= date('d F Y', $u->user_dateCreated); ?></td>
                <td>
                  <a href="<?= base_url('pegawai/detailPegawai/'.$u->user_id); ?>" class="btn btn-info btn-sm mb-2">Detail</a>
                  <a href="<?= base_url('pegawai/editPegawai/'.$u->user_id); ?>" class="btn btn-primary btn-sm mb-2">Edit</a>
                  <a href="<?= base_url('pegawai/hapusPegawai/'.$u->user_id); ?>" class="btn btn-danger btn-sm">Hapus</a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>

        <h4 class="mt-5 mb-3">Nomor Refrensi</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <tr>
              <th>No</th>
              <th>Nomor Refrensi</th>
              <th>Role</th>
            </tr>
            <?php $no = 1; foreach($noRef as $n) { ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= $n->no_ref; ?></td>
              <td><?php
                if($n->no_role == 1) {
                  echo '<div class="badge badge-info">Kepala Bagian</div>';
                } elseif($n->no_role == 2) {
                  echo '<div class="badge badge-info">SDM</div>';
                } elseif($n->no_role == 3) {
                  echo '<div class="badge badge-info">Pegawai</div>';
                }
              ?></td>
            </tr>
            <?php } ?>
          </table>
        </div>
        <?php } elseif($user['user_role'] == 1 && $d->divisi_nama == $user['user_divisi']) { ?>
        <div class="table-responsive">
          <table class="table table-bordered table-hover d-block mx-auto" id="data">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama Pegawai</td>
                <td>Nomor NIK</td>
                <td>Email pegawai</td>
                <td>Divisi</td>
                <td>Jabatan</td>
                <td>Tanggal Masuk Perusahaan</td>
                <td>Tanggal Buat Akun</td>
                <td>Opsi</td>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; 
              $w = $user['user_divisi'];
                $sql = $this->db->query("SELECT * FROM tb_user WHERE user_divisi='$w'")->result();
                foreach($sql as $u) {
              ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?= $u->user_nama; ?></td>
                <td><?= $u->user_nik; ?></td>
                <td><?= $u->user_email; ?></td>
                <td><?= $u->user_divisi; ?></td>
                <td><?= $u->user_jabatan; ?></td>
                <td><?= date('d M Y', strtotime($u->user_tglMasukPerusahaan)); ?></td>
                <td><?= date('d F Y', $u->user_dateCreated); ?></td>
                <td>
                  <a href="<?= base_url('pegawai/detailPegawai/'.$u->user_id); ?>" class="btn btn-info btn-sm">Detail</a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <?php } } ?>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="add">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Tambah Data Pegawai</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('pegawai'); ?>" method="post" id="myform">
          <div class="form-group">
            <label>Nama pegawai</label>
            <input type="text" name="nama" class="form-control" required>
          </div>
          <div class="form-group">
            <label>NIK pegawai</label>
            <input type="number" name="nik" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Jenis kelamin pegawai</label>
            <select name="jk" class="form-control">
              <option selected disabled>-- Pilih Jenis Kelamin --</option>
              <option value="Laki-laki">Laki-laki</option>
              <option value="Perempuan">Perempuan</option>
            </select>
          </div>
          <div class="form-group">
            <label>Tanggal lahir pegawai</label>
            <input type="date" name="tglLahir" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Email pegawai</label>
            <input type="email" name="email" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Password pegawai</label>
            <input type="password" name="pass" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Divisi pegawai</label>
            <select name="divisi" class="form-control" required>
              <option selected disabled>-- Pilih Divisi --</option>
              <?php foreach($divisi as $d) { ?>
              <option value="<?= $d->divisi_nama; ?>"><?= $d->divisi_nama; ?></option>
              <?php }?>
            </select>
          </div>
          <div class="form-group">
            <label>Jabatan pegawai</label>
            <select name="jabatan" class="form-control" required>
              <option selected disabled>-- Pilih Jabatan --</option>
              <?php foreach($jabatan as $j) { ?>
              <option value="<?= $j->jabatan_nama; ?>"><?= $j->jabatan_nama; ?></option>
              <?php }?>
            </select>
          </div>
          <div class="form-group">
            <label>Tanggal masuk perusahaan</label>
            <input type="date" name="tglMasuk" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Nomor Refrensi</label>
            <input type="number" name="noRef" class="form-control" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" id="kirim" class="btn btn-primary btn-sm">Tambah Karyawan</button>
      </form>
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>