<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="<?= base_url('asset/css/bootstrap.min.css'); ?>">
  <script src="<?= base_url('asset/js/sweet.js'); ?>"></script>
	<script>
		function failed() {
			swal({
					title: "Gagal Registrasi Karyawan",
					text: "Nomor Verifikasi tidak sesuai!",
					icon: "error",
					button: "Tutup",
				});
		}
	</script>

	<title><?= $title; ?></title>
</head>
<body class="bg-primary">

<?= $this->session->flashdata('pesan'); ?>

	<main class="container mt-5">
		<section class="row">
			<article class="col-md-6 mx-auto">
				<div class="card">
					<div class="card-header">
						<h4>Verifikasi Email</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-10 mx-auto">
                <div class="alert alert-info">
                  <p>Nomor Verifikasi sudah dikirim ke alamat email anda</p>
                </div>
                <?= $this->session->flashdata('message'); ?>
								<form action="<?= base_url('verifikasi'); ?>" method="post">
									<div class="form-group">
                    <label>Masukan Nomor Verifikasi</label>
                    <input type="number" name="token" class="form-control">
                    <?= form_error('token','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <button type="submit" class="btn btn-primary btn-sm">Verifikasi</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</article>
		</section>
  </main>
  

  <script src="<?= base_url('asset/js/jquery.js'); ?>"></script>	
	<script src="<?= base_url('asset/js/bootstrap.min.js'); ?>"></script>

</body>
</html>