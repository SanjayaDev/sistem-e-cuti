<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0.0 Beta
    </div>
    <strong>Copyright &copy; <?= date('Y'); ?> PT Amarta Karya</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- DataTables jQuery Bootstrap 4 -->
<script src="<?= base_url('vendor/'); ?>plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('vendor/'); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('vendor/'); ?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('vendor/'); ?>dist/js/demo.js"></script>
<!-- DataTables Bootstrap 4 -->
<script src="<?= base_url('vendor/'); ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<!-- Datatable -->
<script src="<?= base_url('asset/js/datatable.js');?>"></script>
</body>
</html>