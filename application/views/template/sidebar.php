<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url('user'); ?>" class="brand-link">
      <img src="<?= base_url('img/amka.jpg'); ?>"
           alt="PT Amarta Karya Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Dashboard</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= base_url('img/'.$user['user_nik']."/").$user['user_img']; ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $user['user_nama']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <?php
          $role = $this->session->userdata('role');
          $query = "SELECT tb_usermenu.menu_id,menu_nama
                    FROM tb_usermenu JOIN tb_accessmenu
                    ON tb_usermenu.menu_id=tb_accessmenu.menu_id
                    WHERE tb_accessmenu.role_id= '$role'
                    ORDER BY tb_accessmenu.menu_id ASC";
          $sql = $this->db->query($query)->result_array();

          foreach($sql as $m) :
        ?>
        
          <li class="nav-header"><?= $m['menu_nama']; ?></li>

          <?php
            $menuId = $m['menu_id'];
            $div = $this->session->userdata('divisi');

            $querySub = "SELECT * 
                        FROM tb_submenu JOIN tb_usermenu
                        ON tb_submenu.menu_id=tb_usermenu.menu_id
                        WHERE tb_submenu.menu_id='$menuId'
                        AND tb_submenu.sub_active='1'
                        AND tb_submenu.divisi_id='$div'
                        ";
            $sub = $this->db->query($querySub)->result_array();

            foreach($sub as $s) :
          ?>

          <li class="nav-item">
            <?php if($title == $s['sub_title']) : ?>
            <a href="<?= base_url($s['sub_url']); ?>" class="nav-link active">
            <?php else : ?>
            <a href="<?= base_url($s['sub_url']); ?>" class="nav-link">
            <?php endif; ?>
              <i class="<?= $s['sub_icon']; ?>"></i>
              <p><?= $s['sub_title']; ?></p>
            </a>
          </li>

          <?php endforeach; ?>

        <?php endforeach; ?>

          <li class="nav-header">Logout</li>
          <li class="nav-item">
            <a href="" class="nav-link" data-target="#logout" data-toggle="modal">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <div class="modal fade" id="logout">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <h5>Yakin ingin keluar?</h5>
        </div>
        <div class="modal-footer">
          <a href="<?= base_url('login'); ?>" class="btn btn-danger btn-sm">Logout</a>
          <button type="button" class="btn btn-info btn-sm" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>