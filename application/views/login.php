<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="<?= base_url('asset/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('asset/font-awesome/css/all.min.css'); ?>">
	<script src="<?= base_url('asset/js/sweet.js'); ?>"></script>
	<script src="<?= base_url('asset/js/new.js'); ?>"></script>
	<title><?= $title; ?></title>
</head>
<body class="bg-primary">

<?= $this->session->flashdata('pesan'); ?>
<?php $this->session->sess_destroy(); ?>

	<main class="container mt-5">
		<section class="row">
			<article class="col-md-4 mx-auto">
				<div class="card">
					<div class="card-header">
						<h4>E-Cuti PT. Amka Indonesia</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-10 mx-auto">
								<form action="" method="post">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-envelope"></i></span>
										</div>
										<input type="email" class="form-control" placeholder="Masukan Email" name="email">
									</div>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-key"></i></span>
										</div>
										<input type="password" class="form-control" placeholder="Masukan Email" name="password" id="pass">
									</div>
									<div class="form-check mb-3">
										<label class="form-check-label">
											<input type="checkbox" class="form-check-input" id="cek">Tampilkan Password
										</label>
									</div>
									<div class="row">
										<div class="col-4">
											<button type="submit" class="btn btn-primary btn-sm">Login</button>
										</div>
										<div class="col-8 text-right">
											<a href="">Lupa Password?</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= base_url('register'); ?>"">Daftar Baru</a>
					</div>
				</div>
			</article>
		</section>
	</main>




	<script src="<?= base_url('asset/js/jquery.js'); ?>"></script>	
	<script src="<?= base_url('asset/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('asset/font-awesome/js/all.min.js') ?>"></script>
	<script>
		$(document).ready(function() {
			$('#cek').click(function(){
				if($(this).is(':checked')) {
					$('#pass').attr('type','text');
				} else {
					$('#pass').attr('type','password');
				}
			})
		});
	</script>
</body>
</html>