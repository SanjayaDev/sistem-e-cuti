<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="<?= base_url('asset/css/bootstrap.min.css'); ?>">
	<script src="<?= base_url('asset/js/sweet.js'); ?>"></script>
	<script>
		function failed() {
			swal({
					title: "Tidak masuk!",
					text: "Anda perlu registrasi terlebih dahulu",
					icon: "error",
					button: "Tutup",
				});
		}
	</script>

	<title><?= $title; ?></title>
</head>
<body class="bg-primary">

<?php $this->session->sess_destroy(); ?>

	<main class="container mt-5">
		<section class="row">
			<article class="col-md-8 mx-auto">
				<div class="card mb-5">
					<div class="card-header">
						<h4>Register - PT. Amka Indonesia</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-10 mx-auto">
							<?= $this->session->flashdata('pesan'); ?>
								<form action="<?= base_url('register') ?>" method="post">
                  <div class="form-group">
                    <label>Nama Karyawan</label>
                    <input type="text" name="nama" class="form-control" value="<?= set_value('nama'); ?>" placeholder="Masukan Nama Karyawan">
                    <?= form_error('nama','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <div class="form-group">
                    <label>Nomor Induk Karyawan</label>
                    <input type="text" name="nik" class="form-control" value="<?= set_value('nik'); ?>" placeholder="Masukan Nomor Induk Karyawan">
                    <?= form_error('nik','<small class="text-danger pl-3">','</small>'); ?>
									</div>
									<div class="form-group">
										<label>Jenis Kelamin</label>
										<select name="jk" class="form-control">
											<option selected disabled>-- Pilih Jenis Kelamin --</option>
											<option value="Laki-laki">Laki-laki</option>
											<option value="Perempuan">Perempuan</option>
										</select>
										<?= form_error('jk','<small class="text-danger pl-3">','</small>'); ?>
									</div>
									<div class="form-group">
										<label>Tanggal Lahir</label>
										<input type="date" name="tglLahir" class="form-control">
										<?= form_error('tglLahir','<small class="text-danger pl-3">','</small>'); ?>
									</div>
                  <div class="form-group">
                    <label>Divisi</label>
                    <select name="divisi" class="form-control">
                      <option disabled selected>-- Pilih Divisi Karyawan --</option>
                      <?php foreach($divisi as $d) : ?>
                      <option value="<?= $d->divisi_nama; ?>"><?= $d->divisi_nama; ?></option>
                      <?php endforeach; ?>
										</select>
										<?= form_error('divisi','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <div class="form-group">
                    <label>Jabatan</label>
                    <select name="jabatan" class="form-control">
                      <option disabled selected>-- Pilih Jabatan Karyawan --</option>
                      <?php foreach($jabatan as $j) : ?>
                      <option value="<?= $j->jabatan_nama; ?>"><?= $j->jabatan_nama; ?></option>
                      <?php endforeach; ?>
										</select>
										<?= form_error('jabatan','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <div class="form-group">
                    <label>Tanggal Masuk Perusahaan</label>
										<input type="date" name="tglMasuk" class="form-control">
										<?= form_error('tglMasuk','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <div class="form-group">
                    <label>Email</label>
										<input type="email" name="email" class="form-control" placeholder="Masukan Email yang aktif" value="<?= set_value('email'); ?>">
										<?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <div class="form-group">
                    <label>Password</label>
										<input type="password" name="pass" class="form-control" placeholder="Buat Password Baru">
										<?= form_error('pass','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <div class="form-group">
                    <label>Nomor Refrensi</label>
										<input type="number" name="ref" class="form-control" placeholder="Masukan Nomor Refrensi" value="<?= set_value('ref'); ?>">
										<?= form_error('ref','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                  <button type="submit" class="btn btn-primary btn-sm">Register</button>
								</form>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?= base_url('login'); ?>" class=" text-decoration-none">Kembali ke Halaman Login</a>
					</div>
				</div>
			</article>
		</section>
	</main>



	<script src="<?= base_url('asset/js/jquery.js'); ?>"></script>	
	<script src="<?= base_url('asset/js/bootstrap.min.js'); ?>"></script>

</body>
</html>