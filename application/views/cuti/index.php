<?= $this->session->flashdata('pesan'); ?>
<div class="content-wrapper">  
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Cuti</h1>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="card">
      <div class="card-body">
      <?php
        foreach($divisi as $d) {
          if($user['user_divisi'] == 'Departmen Sumber Daya Manusia' && $user['user_role'] == 2 && $d->divisi_nama == "Departmen Sumber Daya Manusia") {
      ?>
        <h4 class="mb-3">Data Cuti Seluruh Divisi</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-hover d-block mx-auto" id="data1">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama Pegawai</td>
                <td>Tahun Cuti</td>
                <td>Jatah Awal Cuti</td>
                <td>Permohonan Hari Cuti</td>
                <td>Tanggal Mulai Cuti</td>
                <td>Tanggal Selesai Cuti</td>
                <td>Tanggal Masuk Kerja Kembali</td>
                <td>Keperluan Cuti</td>
                <td>Divisi</td>
                <td>Sisa Cuti</td>
                <td>Status Approved</td>
                <td>Detail</td>
              </tr>
            </thead>
            <tbody>
              <?php
                $no = 1;
                $u = $user['user_divisi'];
                $data = $this->m_cuti->getData('tb_datacuti')->result();
                foreach($data as $d) {
                  $usr = $d->user_id;
                  $p = $this->m_cuti->editData(['user_id' => $d->user_id],'tb_user')->row();
                  
                  $sisa = $this->db->query("SELECT * FROM tb_sisacuti,tb_jatahcuti WHERE tb_sisacuti.usr_id='$usr'")->row();
              ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?= $p->user_nama; ?></td>
                <td><?= $sisa->jth_tahun; ?></td>
                <td><?= $d->cuti_awal; ?> Hari</td>
                <td><?= $d->cuti_hari; ?> Hari</td>
                <td><?= date('d M Y', strtotime($d->cuti_dariTgl)); ?></td>
                <td><?= date('d M Y', strtotime($d->cuti_sampaiTgl)); ?> </td>
                <td><?= date('d M Y', strtotime($d->cuti_tglMasukKerja)); ?> </td>
                <td><?= $d->cuti_keperluan; ?> </td>
                <td><?= $p->user_divisi; ?> </td>
                <td><?= $d->cuti_sisa; ?> Hari</td>
                <td>
                <?php
                  if($d->cuti_statusApprov == 0) {
                    echo '<div class="badge badge-warning">Waiting Approved</div>';
                  } elseif($d->cuti_statusApprov == 1) {
                    echo '<div class="badge badge-success">Approved</div>';
                  } else {
                    echo '<div class="badge badge-danger">Rejected</div>';
                  }
                ?>
                </td>
                <td>
                  <a href="<?= base_url('cuti/detailCuti/'.$d->cuti_id); ?>" class="btn btn-info btn-sm">Detail</a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>     
        </div>

      <?php } elseif($user['user_divisi'] == 'Departmen Sumber Daya Manusia' && $user['user_role'] == 1 && $d->divisi_nama == "Departmen Sumber Daya Manusia") {  ?>
       
        <h4 class="mb-3">Data Cuti Seluruh Divisi</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-hover d-block mx-auto" id="data1">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama Pegawai</td>
                <td>Tahun Cuti</td>
                <td>Jatah Awal Cuti</td>
                <td>Permohonan Hari Cuti</td>
                <td>Tanggal Mulai Cuti</td>
                <td>Tanggal Selesai Cuti</td>
                <td>Tanggal Masuk Kerja Kembali</td>
                <td>Keperluan Cuti</td>
                <td>Divisi</td>
                <td>Sisa Cuti</td>
                <td>Status Approved</td>
                <td>Detail</td>
              </tr>
            </thead>
            <tbody>
              <?php
                $no = 1;
                $u = $user['user_divisi'];
                $data = $this->db->query("SELECT * FROM tb_datacuti WHERE cuti_divisi!='$u'")->result();
                foreach($data as $d) {
                  $usr = $d->user_id;
                  $p = $this->m_cuti->editData(['user_id' => $d->user_id],'tb_user')->row();
                  
                  $sisa = $this->db->query("SELECT * FROM tb_sisacuti,tb_jatahcuti WHERE tb_sisacuti.usr_id='$usr'")->row();
              ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?= $p->user_nama; ?></td>
                <td><?= $sisa->jth_tahun; ?></td>
                <td><?= $d->cuti_awal; ?> Hari</td>
                <td><?= $d->cuti_hari; ?> Hari</td>
                <td><?= date('d M Y', strtotime($d->cuti_dariTgl)); ?></td>
                <td><?= date('d M Y', strtotime($d->cuti_sampaiTgl)); ?> </td>
                <td><?= date('d M Y', strtotime($d->cuti_tglMasukKerja)); ?> </td>
                <td><?= $d->cuti_keperluan; ?> </td>
                <td><?= $p->user_divisi; ?> </td>
                <td><?= $d->cuti_sisa; ?> Hari</td>
                <td>
                <?php
                  if($d->cuti_statusApprov == 0) {
                    echo '<div class="badge badge-warning">Waiting Approved</div>';
                  } elseif($d->cuti_statusApprov == 1) {
                    echo '<div class="badge badge-success">Approved</div>';
                  } else {
                    echo '<div class="badge badge-danger">Rejected</div>';
                  }
                ?>
                </td>
                <td>
                  <a href="<?= base_url('cuti/detailCuti/'.$d->cuti_id); ?>" class="btn btn-info btn-sm">Detail</a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>     
        </div>
      <?php } elseif($user['user_role'] == 1 && $d->divisi_nama == $user['user_divisi']) { ?>
        <a href="" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#Add">Tambah Data</a>
        <div class="table-responsive">
          <table class="table table-bordered table-hover d-block mx-auto" id="data">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama Pegawai</td>
                <td>Tahun Cuti</td>
                <td>Jatah Awal Cuti</td>
                <td>Permohonan Hari Cuti</td>
                <td>Tanggal Mulai Cuti</td>
                <td>Tanggal Selesai Cuti</td>
                <td>Tanggal Masuk Kerja Kembali</td>
                <td>Keperluan Cuti</td>
                <td>Divisi</td>
                <td>Sisa Cuti</td>
                <td>Opsi</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <?php
                $no = 1;
                $u = $user['user_divisi'];
                $data = $this->db->query("SELECT * FROM tb_datacuti WHERE cuti_divisi='$u'")->result();
                foreach($data as $s) {
                  $usr = $s->user_id;
                  
                  $sisa = $this->db->query("SELECT * FROM tb_sisacuti,tb_jatahcuti WHERE tb_sisacuti.usr_id='$usr'")->row();
                  $p = $this->m_cuti->editData(['user_id' => $usr],'tb_user')->row();
              ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?= $p->user_nama; ?></td>
                <td><?= $sisa->jth_tahun; ?></td>
                <td><?= $s->cuti_awal; ?> Hari</td>
                <td><?= $s->cuti_hari; ?> Hari</td>
                <td><?= date('d M Y', strtotime($s->cuti_dariTgl)); ?></td>
                <td><?= date('d M Y', strtotime($s->cuti_sampaiTgl)); ?> </td>
                <td><?= date('d M Y', strtotime($s->cuti_tglMasukKerja)); ?> </td>
                <td><?= $s->cuti_keperluan; ?> </td>
                <td><?= $s->cuti_divisi; ?> </td>
                <td><?= $s->cuti_sisa; ?> Hari</td>
                <td>
                  <?php
                    if($s->cuti_statusApprov == 0) {
                  ?>
                  <a href="<?= base_url('cuti/detailCuti/'.$s->cuti_id); ?>" class="btn btn-info btn-sm mb-2">Detail</a>
                  <a href="<?= base_url('cuti/approv/'.$s->cuti_id); ?>" class="btn btn-success btn-sm mb-2">Approv</a>
                  <a href="<?= base_url('cuti/reject/'.$s->cuti_id); ?>" class="btn btn-danger btn-sm">Reject</a>
                    <?php } else { 
                      if($s->cuti_statusApprov == 1) {
                        echo '<div class="badge badge-success">Approved</div>';
                        echo '<a href="'.base_url('cuti/detailCuti/'.$s->cuti_id).'"class="btn btn-info btn-sm mt-2">Detail</a>';
                      } else {
                        echo '<div class="badge badge-danger">Rejected</div>';
                        echo '<a href="'.base_url('cuti/detailCuti/'.$s->cuti_id).'"class="btn btn-info btn-sm mt-2">Detail</a>';
                      }
                    } ?>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="modal fade" id="Add">
          <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
              <div class="modal-header">
                <h4>Tambah Cuti Karyawan</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
              </div>
              <div class="modal-body">
                <form action="<?= base_url('cuti'); ?>" method="post">
                  <div class="form-group">
                    <label>Masukan NIK Pegawai</label>
                    <input type="number" name="nik" class="form-control" required>
                  </div>
                  <div class="form-group">
                    <label>Hari Cuti Pegawai</label>
                    <input type="number" name="hcuti" class="form-control" required>
                  </div>
                  <div class="form-group">
                    <label>Tanggal Mulai Cuti Pegawai</label>
                    <input type="date" name="tglMulai" class="form-control" required>
                  </div>
                  <div class="form-group">
                    <label>Tanggal Selesai Cuti Pegawai</label>
                    <input type="date" name="tglSelesai" class="form-control" required>
                  </div>
                  <div class="form-group">
                    <label>Tanggal Mulai Kerja Pegawai</label>
                    <input type="date" name="tglKerja" class="form-control" required>
                  </div>
                  <div class="form-group">
                    <label>Alasan Cuti</label>
                    <input type="text" name="alasan" class="form-control" required>
                  </div>
                  <input type="submit" value="Simpan" class="btn btn-primary btn-sm">
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      <?php } } ?>
      </div>
    </div>
  </section>
</div>