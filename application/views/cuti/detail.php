<div class="content-wrapper">  
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Detail Data Cuti</h1>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="card">
      <div class="card-body">
        <h4 class="text-center mb-5">Data Karyawan</h4>
        <div class="row mb-5">
          <div class="col-md-4 mx-auto">
            <img src="<?= base_url('img/'.$usr->user_nik."/".$usr->user_img); ?>" class="img-cuti">
          </div>
          <div class="col-md-8">
            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                  <th>NIK Karyawan</th>
                  <td>: <?= $usr->user_nik; ?></td>
                </tr>
                <tr>
                  <th>Nama Karyawan</th>
                  <td>: <?= $usr->user_nama; ?></td>
                </tr>
                <tr>
                  <th>Jenis Kelamin Karyawan</th>
                  <td>: <?= $usr->user_jk; ?></td>
                </tr>
                <tr>
                  <th>Tanggal Lahir Karyawan</th>
                  <td>: <?= date('d M Y', strtotime($usr->user_tglLahir)); ?></td>
                </tr>
                <tr>
                  <th>Divisi</th>
                  <td>: <?= $usr->user_divisi; ?></td>
                </tr>
                <tr>
                  <th>Jabatan</th>
                  <td>: <?= $usr->user_jabatan; ?></td>
                </tr>
                <tr>
                  <th>Sisa Cuti</th>
                  <td>: <?= $sisa->sisa_pemakaian; ?> Hari</td>
                </tr>
              </table>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <h4 class="text-center mb-5">Data Cuti Karyawan</h4>
            <div class="table-responsive">
              <table class="table table-bordered table-hover d-block mx-auto" id="data">
                <thead>
                  <tr>
                    <td>No</td>
                    <td>Nama Pegawai</td>
                    <td>Tahun Cuti</td>
                    <td>Jatah Awal Cuti</td>
                    <td>Permohonan Hari Cuti</td>
                    <td>Tanggal Mulai Cuti</td>
                    <td>Tanggal Selesai Cuti</td>
                    <td>Tanggal Masuk Kerja Kembali</td>
                    <td>Keperluan Cuti</td>
                    <td>Divisi</td>
                    <td>Sisa Cuti</td>
                    <td>Status Approved</td>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $no = 1;
                    foreach($cuti as $d) {  
                      $user = $d->user_id;  
                      $sisa = $this->db->query("SELECT * FROM tb_sisacuti,tb_jatahcuti WHERE usr_id='$user'")->row();                  
                  ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $d->user_nama; ?></td>
                    <td><?= $sisa->jth_tahun; ?></td>
                    <td><?= $d->cuti_awal; ?> Hari</td>
                    <td><?= $d->cuti_hari; ?> Hari</td>
                    <td><?= date('d M Y', strtotime($d->cuti_dariTgl)); ?></td>
                    <td><?= date('d M Y', strtotime($d->cuti_sampaiTgl)); ?> </td>
                    <td><?= date('d M Y', strtotime($d->cuti_tglMasukKerja)); ?> </td>
                    <td><?= $d->cuti_keperluan; ?> </td>
                    <td><?= $d->cuti_divisi; ?> </td>
                    <td><?= $d->cuti_sisa; ?> Hari</td>
                    <td>
                    <?php
                    if($d->cuti_statusApprov == 0) {
                      echo '<div class="badge badge-warning">Waiting Approved</div>';
                    } elseif($d->cuti_statusApprov == 1) {
                      echo '<div class="badge badge-success">Approved</div>';
                    } else {
                      echo '<div class="badge badge-danger">Rejected</div>';
                    }
                  ?>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>