<div class="content-wrapper">  
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Sisa Cuti</h1>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="card">
      <div class="card-body">
        <h4 class="text-center mb-4">Data sisa cuti karyawan</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-hover d-block mx-auto" id="data">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama Karyawan</td>
                <td>NIP</td>
                <td>Divisi</td>
                <td>Jabatan</td>
                <td>Tahun</td>
                <td>Sisa Cuti</td>
              </tr>
              <tbody>
                <?php $no=1; foreach($sisa as $s) : ?>
                <tr>
                  <td><?= $no++; ?></td>
                  <td><?= $s->user_nama; ?></td>
                  <td><?= $s->user_nik; ?></td>
                  <td><?= $s->user_divisi; ?></td>
                  <td><?= $s->user_jabatan; ?></td>
                  <td><?= $s->jth_tahun; ?></td>
                  <td><?= $s->sisa_pemakaian; ?> Hari</td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </thead>
          </table>
        </div>
        <div class="table-responsive">
          <h4 class="mt-5 mb-3">Jatah tahunan cuti</h4>
          <a href="" class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#add">Tambah jatah tahunan</a>
          <table class="table table-bordered table-hover d-block mx-auto" id="data1">
            <thead>
              <tr>
                <td>No</td>
                <td>Tahun</td>
                <td>Jatah</td>
                <td>Opsi</td>
              </tr>
              <tbody>
                <?php $no=1; foreach($tahun as $t) : ?>
                <tr>
                  <td><?= $no++; ?></td>
                  <td><?= $t->jth_tahun; ?></td>
                  <td><?= $t->jth_hari; ?> Hari</td>
                  <td>
                    <a href="<?= base_url('cuti/hapusJatah/'.$t->jth_id); ?>" class="btn btn-outline-danger btn-sm">Hapus</a>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="add">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Tambah Cuti Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('cuti/sisaCuti'); ?>" method="post">
          <div class="form-group">
            <label>Tahun</label>
            <input type="number" name="tahun" class="form-control">
          </div>
          <div class="form-group">
            <label>Jatah Cuti (Hari)</label>
            <input type="number" name="jatah" class="form-control">
          </div>
          <input type="submit" value="Simpan" class="btn btn-primary btn-sm">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>