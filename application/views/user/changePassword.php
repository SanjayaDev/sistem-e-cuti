<?= $this->session->flashdata('pesan'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h4>Ubah Password</h4>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-sm-6">
        <div class="card">
          <div class="card-body">
            <form action="changePassword" method="post">
              <div class="form-group">
                <label>Masukan Password Lama</label>
                <input type="password" name="passLama" class="form-control">
                <?= form_error('passLama', '<small class="text-danger">','</small>'); ?>
              </div>
              <div class="form-group">
                <label>Masukan Password Baru</label>
                <input type="password" name="passBaru" class="form-control">
                <?= form_error('passBaru', '<small class="text-danger">','</small>'); ?>
              </div>
              <div class="form-group">
                <label>Tulis Password Baru Kembali</label>
                <input type="password" name="passBaru1" class="form-control">
                <?= form_error('passBaru1', '<small class="text-danger">','</small>'); ?>
              </div>
              <input type="submit" value="Simpan" class="btn btn-primary btn-sm">
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>