<?= $this->session->flashdata('pesan'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h4>Edit Profile Saya</h4>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-sm-8">
        <div class="card">
          <div class="card-body">
          <form action="<?= base_url('user/editProfile'); ?>" enctype="multipart/form-data" method="post">
              <div class="form-group">
                <label>Nama Karyawan</label>
                <input type="hidden" name="id" value="<?= $user['user_id']; ?>">
                <input type="text" name="nama" class="form-control" required value="<?= $user['user_nama']; ?>">
                <?= form_error('nama', '<small class="text-danger">','</small>') ?>
              </div>
              <div class="form-group">
                <label>Jenis Kelamin</label>
                <select name="jk" class="form-control" required>
                  <option selected disabled>-- Pilih Jenis Kelamin</option>
                  <option <?php if($user['user_jk'] == "Laki-laki") { echo "selected"; } ?> value="Laki-laki">Laki-laki</option>
                  <option <?php if($user['user_jk'] == "Perempuan") { echo "selected"; } ?> value="Perempuan">Perempuan</option>
                </select>
                <?= form_error('jk', '<small class="text-danger">','</small>') ?>
              </div>
              <div class="form-group">
                <label>Tanggal Lahir</label>
                <input type="date" name="tglLahir" class="form-control" required>
                <?= form_error('tglLahir', '<small class="text-danger">','</small>') ?>
              </div>
              <div class="form-group">
                <label>Email Karyawan</label>
                <input type="email" name="email" class="form-control" required value="<?= $user['user_email']; ?>">
                <?= form_error('email', '<small class="text-danger">','</small>') ?>
              </div>
              <div class="form-group">
                <label>Divisi Karyawan</label>
                <select name="divisi" class="form-control" >
                  <option selected disabled><?= $user['user_divisi']; ?></option>
                </select>
              </div>
              <div class="form-group">
                <label>Jabatan Karyawan</label>
                <select name="divisi" class="form-control">
                  <option selected disabled><?= $user['user_jabatan']; ?></option>
                </select>
              </div>
              <img src="<?= base_url('img/').$user['user_img']; ?>" alt="Profile" width="20%">
              <div class="form-group">
                <label>Pilih Gambar</label>
                <input type="file" name="gbr" class="form-control" required>
                <?= form_error('gbr', '<small class="text-danger">','</small>') ?>
              </div>
              <input type="submit" value="Simpan" class="btn btn-primary btn-sm" formnovalidate="formnovalidate">
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>