<?= $this->session->flashdata('pesan'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h4>Profile Saya</h4>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-sm-8">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <img src="<?= base_url('img/'.$user['user_nik']."/").$user['user_img']; ?>" class="mb-3 d-block mx-auto" id="img" alt="Foto User">
              </div>
              <div class="col-sm-8">
                <table class="table table-hover">
                  <tr>
                    <th>NIP</th>
                    <td>: <?= $user['user_nik']; ?></td>
                  </tr>
                  <tr>
                    <th>Nama</th>
                    <td>: <?= $user['user_nama']; ?></td>
                  </tr>
                  <tr>
                    <th>Jenis Kelamin</th>
                    <td>: <?= $user['user_jk']; ?></td>
                  </tr>
                  <tr>
                    <th>Tanggal Lahir</th>
                    <td>: <?= date('d M Y', strtotime($user['user_tglLahir'])); ?></td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td>: <?= $user['user_email']; ?></td>
                  </tr>
                  <tr>
                    <th>Divisi</th>
                    <td>: <?= $user['user_divisi']; ?></td>
                  </tr>
                  <tr>
                    <th>Jabatan</th>
                    <td>: <?= $user['user_jabatan']; ?></td>
                  </tr>
                  <tr>
                    <th>Tanggal Masuk Perusahaan</th>
                    <td>: <?= date('d-M-Y', strtotime($user['user_tglMasukPerusahaan'])); ?></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>