<?= $this->session->flashdata('pesan'); ?>
<div class="content-wrapper">  
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Ajukan Cuti</h1>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="card">
      <div class="card-body">
        <p>
          Sisa jatah cuti kamu tahun ini adalah <?= $cuti->sisa_pemakaian; ?> Hari
        </p>
        <a href="" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#add">Ajukan Cuti</a>
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="data">
            <thead>
              <tr>
                <td>No</td>
                <td>Permohonan Hari Cuti</td>
                <td>Alasan Cuti</td>
                <td>Tanggal Mulai Cuti</td>
                <td>Tanggal Selesai Cuti</td>
                <td>Tanggal Masuk Kerja</td>
                <td>Status</td>
                <td>Cetak</td>
              </tr>
            </thead>
            <tbody>
              <?php
                $no = 1;
                $usr =  $this->session->userdata('id');
                
                $query = "SELECT * FROM tb_datacuti WHERE user_id='$usr'";
                $sql = $this->db->query($query)->result();

                foreach($sql as $s) {
                  $sisa = $this->db->query("SELECT * FROM tb_sisacuti,tb_jatahcuti WHERE usr_id='$usr'")->row();
              ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?= $s->cuti_hari; ?> Hari</td>
                <td><?= $s->cuti_keperluan; ?></td>
                <td><?= date('d M Y', strtotime($s->cuti_dariTgl)); ?></td>
                <td><?= date('d M Y', strtotime($s->cuti_sampaiTgl)); ?></td>
                <td><?= date('d M Y', strtotime($s->cuti_tglMasukKerja)); ?></td>
                <td>
                  <?php
                    if($s->cuti_statusApprov == 0) {
                      echo '<div class="badge badge-warning">Waiting Approved</div>';
                    } elseif($s->cuti_statusApprov == 1) {
                      echo '<div class="badge badge-info">Approved</div>';
                    } elseif($s->cuti_statusApprov == 2) {
                      echo '<div class="badge badge-danger">Rejected</div>';
                    }
                  ?>
                </td>
                <td>
                	<a href="<?= base_url('ajukancuti/cetak/'.$s->cuti_id); ?>" class="btn btn-success btn-sm" target="_blank">Cetak</a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="add">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Tambah Cuti</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('ajukancuti'); ?>" method="post">
          <div class="form-group">
            <label>Permohonan Hari Cuti</label>
            <input type="number" name="hcuti" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Tanggal Mulai Cuti Pegawai</label>
            <input type="date" name="tglMulai" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Tanggal Selesai Cuti Pegawai</label>
            <input type="date" name="tglSelesai" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Tanggal Mulai Kerja Pegawai</label>
            <input type="date" name="tglKerja" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Alasan Cuti</label>
            <input type="text" name="alasan" class="form-control" required>
          </div>
          <input type="submit" value="Simpan" class="btn btn-primary btn-sm">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>