<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="<?= base_url('asset/css/style.css'); ?>">
  <title>Cetak Surat Permohonan</title>
</head>
<body>

  <div class="container">
    <h3>PERMOHONAN PEMANFAATAN HAK CUTI TAHUNAN</h3>
    <table>
      <tr>
        <td>1</td>
        <td>Nama Pemohon</td>
        <td class="dotted">: </td>
      </tr>
      <tr>
        <td>2</td>
        <td>Satuan Kerja</td>
        <td class="dotted">: </td>
      </tr>
      <tr>
        <td>3</td>
        <td>Jabatan</td>
        <td class="dotted">: </td>
      </tr>
      <tr>
        <td>4</td>
        <td>Status</td>
        <td class="dotted">: </td>
      </tr>
      <tr>
        <td>5</td>
        <td>Tanggal Masuk AMKA</td>
        <td class="dotted">: </td>
      </tr>
      <tr>
        <td>6</td>
        <td>Hak Cuti Tahun</td>
        <td class="dotted">: </td>
      </tr>
      <tr>
        <td>7</td>
        <td>Keperluan Cuti</td>
        <td class="dotted">: </td>
      </tr>
      <tr>
        <td>8</td>
        <td>Selama Cuti Berada</td>
        <td class="dotted">: </td>
      </tr>
      <tr>
        <td>9</td>
        <td>PJ Pekerjaan Selama Cuti</td>
        <td class="dotted">: </td>
      </tr>
      <tr>
        <td>10</td>
        <td>Tanda Tangan Persetujuan Penanggung Jawab Pekerjaan Selama Cuti</td>
        <td class="kotak"></td>
      </tr>
    </table>
    <p>Dengan ini kami mengajukan pemohonan untuk dapat diperkenankan memanfaatkan
        Hak Cuti Tahunan kami selama …….hari kerja dari tanggal …………..s.d. ……..… 20 …
        Atas perkenan dan perhatian Bapak, kami ucapkan terima kasih.</p>
    <hr>
    <h4>Keterangan Jumlah :</h4>
    <table>
      <tr>
        <td>Hak Cuti</td>
        <td>: <?= $s->sisa_awal; ?> Hari</td>
      </tr>
      <tr>
        <td>Cuti yang sudah dilaksanakan</td>
        <td>: <?php $d= $s->sisa_awal-$s->sisa_pemakaian; 
          	echo $d; ?> Hari</td>
      </tr>
      <tr>
        <td>Permohonan cuti</td>
        <td class="solid">: <span class="kanan">(-)</span></td>
      </tr>
      <tr>
        <td>Sisa hak cuti</td>
        <td class="dotted">: </td>
      </tr>
    </table>
    <hr>
    <h3>PERSETUJUAN KEPALA SATUAN KERJA</h3>
    <P>Atas dasar pertimbangan keperluan Cuti Tahunan bagi pemohon, terhadap permohonan
        pemanfaatan Hak Cuti Tahunan diatas, kami berpendapat bahwa permohonan
        pemanfaatan cuti selama ……. hari kerja yaitu dari tanggal : ………………………..
        s.d. ……………………….. tersebut TIDAK / DAPAT disetujui.</P>
    <p>Demikian agar maklum dan untuk dilaksanakan sebagaimana mestinya.</p>
    <p class="kanan">
        ................,........................... 20....
    </p>

    <table class="table">
      <tr>
        <td class="solid-kanan"><p>Diketahui <br>
            Divisi HC & Pengembangan <br>
            Bisnis</p>
            <br><br><br><br>
            <p>(Kepala HC&PB)</p>
            <p>Kepala</p>
        </td>
        <td class="solid-kanan">
          <p>Disetujui,</p><p>...............................,</p>
          <br><br><br><br><br>
          <p>(...........................)</p>
          <p>kepala</p>
        </td>
        <td class="text-center">
          <br>
          <p>Pemohon</p><p>.......................,</p>
          <br><br><br><br><br>
          <p>(..................)</p>
          <br><br><br>
        </td>
      </tr>
    </table>
  </div>
  <script>
    window.print();
  </script>
</body>
</html>