<?php
 function genNoRef() {

  $ref = get_instance();
  $kepalaBagian = rand(1, 10000);
  $kepegawaian = rand(1, 10000);
  $pegawai = rand(1, 10000);

  $where = ['no_hari' => date('D')];

  $cek = $ref->db->get_where('tb_norefrensi',$where)->row_array();


  if(!$cek) {
    $data = [
      'no_ref' => $kepalaBagian,
      'no_hari' => date('D')
    ];
    $where = ['no_role' => 1];
    $ref->db->where($where)->update('tb_norefrensi',$data);

    $data = [
      'no_ref' => $kepegawaian,
      'no_hari' => date('D')
    ];
    $where = ['no_role' => 2];
    $ref->db->where($where)->update('tb_norefrensi',$data);

    $data = [
      'no_ref' => $pegawai,
      'no_hari' => date('D')
    ];
    $where = ['no_role' => 3];
    $ref->db->where($where)->update('tb_norefrensi',$data);
  }
 }
?>