<?php
  function protect()
  {
    $ci = get_instance();
    if(!$ci->session->userdata('email'))
    {
      $ci->session->set_flashdata('pesan', '<script>blocked();</script>');
      redirect('login');
    } else {
      $role_id = $ci->session->userdata('role');
      $url = $ci->uri->segment(1);

      $query = $ci->db->get_where('tb_usermenu', ['menu_nama' => $url])->row_array();
      $id = $query['menu_id'];

      $user = $ci->db->get_where('tb_accessmenu', [
        'role_id' => $role_id, 
        'menu_id' => $id]
      );

      if($user->num_rows() == 0)  
      {
        redirect('auth/blocked');
      } 
    }
  }
?>