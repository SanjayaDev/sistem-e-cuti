<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajukancuti extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('email'))
    {
      $this->session->set_flashdata('pesan', '<script>blocked();</script>');
      redirect('login');
    } else {
      if($this->session->userdata('role') != 3) {
        redirect('auth/blocked');
      }
    }
  }

  public function index()
  {
    $this->form_validation->set_rules('hcuti', 'Hari Cuti', 'required');
    $this->form_validation->set_rules('tglMulai', 'Tanggal Mulai Cuti', 'required');
    $this->form_validation->set_rules('tglSelesai', 'Tanggal Selesai Cuti', 'required');
    $this->form_validation->set_rules('tglKerja', 'Tanggal Mulai Kerja', 'required');
    $this->form_validation->set_rules('alasan', 'Alasan Cuti', 'required');

    
    if ($this->form_validation->run() == FALSE) {
      $data = [
        'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row_array(),
        'title' => 'Ajukan Cuti',
        'cuti' => $this->m_cuti->editData(['usr_id' => $this->session->userdata('id')],'tb_sisacuti')->row()
      ];
      $this->load->view('template/header',$data);
      $this->load->view('template/sidebar',$data);
      $this->load->view('ajukancuti/index',$data);
      $this->load->view('template/footer');
    } else {
      $id = $this->session->userdata('id');
      $hcuti = htmlspecialchars($this->input->post('hcuti'));
      $sql = $this->m_cuti->editData(['usr_id' => $id], 'tb_sisacuti')->row_array();

      if($sql['sisa_pemakaian'] < 1) {
        $this->session->set_flashdata('pesan', '<script>cutiJatah();</script>');
      
        redirect('ajukancuti');
      } else {
        $sisa = $sql['sisa_pemakaian'] - $hcuti;
        if($sisa < 1) {
          $this->session->set_flashdata('pesan', '<script>cutiHabis();</script>');
      
          redirect('ajukancuti');
        } else {
          $this->inputCuti();
        }
      }
    }
  }

  private function inputCuti()
  {
    $id         = $this->session->userdata('id');
    $hcuti      = htmlspecialchars($this->input->post('hcuti'));
    $tglMulai   = htmlspecialchars($this->input->post('tglMulai'));
    $tglSelesai = htmlspecialchars($this->input->post('tglSelesai'));
    $tglKerja   = htmlspecialchars($this->input->post('tglKerja'));
    $alasan     = htmlspecialchars($this->input->post('alasan'));

    $usr = $this->m_cuti->editData(['user_id' => $id], 'tb_user')->row();
    $sisa = $this->m_cuti->editData(['usr_id' => $usr->user_id], 'tb_sisacuti')->row();
    $sPemakaian = $sisa->sisa_pemakaian;

    $sAkhirPemakaian = $sPemakaian - $hcuti;
    $thn = date('Y');
    $thnIni = $this->m_cuti->editData(['jth_tahun' => $thn],'tb_jatahcuti')->row();

    $where = ['usr_id' => $usr->user_id, 'jth_id' => $thnIni->jth_id];
    $data = [
      'sisa_awal' => $sPemakaian,
      'sisa_pemakaian' => $sAkhirPemakaian
    ];
    $this->m_cuti->updateData($data,$where,'tb_sisacuti');
    $sisa = $this->m_cuti->editData(['usr_id' => $usr->user_id], 'tb_sisacuti')->row();

    $data = [
      'user_id' => $id,
      'sisa_id' => $sisa->sisa_id,
      'cuti_hari' => $hcuti,
      'cuti_awal' => $sisa->sisa_awal,
      'cuti_dariTgl' => $tglMulai,
      'cuti_sampaiTgl' => $tglSelesai,
      'cuti_keperluan' => $alasan,
      'cuti_sisa' => $sisa->sisa_pemakaian,
      'cuti_divisi' => $usr->user_divisi,
      'cuti_tglMasukKerja' => $tglKerja,
      'cuti_statusApprov' => 0
    ];

    $this->m_cuti->insertData($data,'tb_datacuti');

    $this->session->set_flashdata('pesan', '<script>cutiSukses();</script>');
    
    redirect('ajukancuti');
  }
  
  public function cetak($id)
  {
    $cuti = $this->m_cuti->editData(['cuti_id' => $id],'tb_datacuti')->row();
    $sisa = $cuti->sisa_id;
  	  $data = [
        'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row_array(),
        's' => $this->db->query("SELECT * FROM tb_datacuti,tb_sisacuti WHERE tb_sisacuti.sisa_id='$sisa'")->row()
      ];
    $this->load->view('ajukancuti/surat',$data);
  }
}