<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		protect(); 
  }

  public function index()
  {
    $this->form_validation->set_rules('nama','Nama Karyawan','required');
    $this->form_validation->set_rules('nik','NIK Karyawan','required');
    $this->form_validation->set_rules('email','Email Karyawan','required|valid_email|is_unique[tb_user.user_email]');
    $this->form_validation->set_rules('pass','Password Karyawan','required');
    $this->form_validation->set_rules('divisi','Divisi Karyawan','required');
    $this->form_validation->set_rules('jabatan','Jabatan Karyawan','required');
    $this->form_validation->set_rules('tglMasuk','Tanggal Masuk Karyawan','required');
    $this->form_validation->set_rules('noRef','Nomor Refrensi Karyawan','required');

    
    if ($this->form_validation->run() == FALSE) {
      $data = [
        'title' => "Data Pegawai",
        'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row_array(),
        'noRef' => $this->m_cuti->getData('tb_norefrensi')->result(),
        'divisi' => $this->m_cuti->getData('tb_divisi')->result(),
        'jabatan' => $this->m_cuti->getData('tb_jabatan')->result(),
      ];
  
      $this->load->view('template/header',$data);
      $this->load->view('template/sidebar',$data);
      $this->load->view('pegawai/index',$data);
      $this->load->view('template/footer');
    } else {
      $this->tambahPegawai();
    }
  }

  private function tambahPegawai()
  {
    $nama       = htmlspecialchars($this->input->post('nama'));
    $nik        = htmlspecialchars($this->input->post('nik'));
    $email      = htmlspecialchars($this->input->post('email'));
    $pass       = password_hash($this->input->post('pass'), PASSWORD_DEFAULT);
    $div        = htmlspecialchars($this->input->post('divisi'));
    $jabatan    = htmlspecialchars($this->input->post('jabatan'));
    $tglMasuk   = htmlspecialchars($this->input->post('tglMasuk'));
    $noRef      = htmlspecialchars($this->input->post('noRef'));
    $tglLahir   = htmlspecialchars($this->input->post('tglLahir'));
    $jk         = htmlspecialchars($this->input->post('jk'));


    $ref = $this->m_cuti->editData(['no_ref' => $noRef], 'tb_norefrensi')->row();

    if($noRef == $ref->no_ref && $ref->no_role == 1) {
      $role = 1;
    } elseif($noRef == $ref->no_ref && $ref->no_role == 2) {
      $role = 2;
    } elseif($noRef == $ref->no_ref && $ref->no_role == 3) {
      $role = 3;
    } else {
      $this->session->set_flashdata('pesan', '<script>noRef();</script>');
      redirect('pegawai');
    }

    $data = [
      'user_nik' => $nik,
      'user_nama' => $nama,
      'user_jk' => $jk,
      'user_tglLahir' => $tglLahir,
      'user_email' => $email,
      'user_password' => $pass,
      'user_divisi' => $div,
      'user_jabatan' => $jabatan,
      'user_tglMasukPerusahaan' => $tglMasuk,
      'user_role' => $role,
      'user_active' => 1,
      'user_dateCreated' => time(),
      'user_img' => 'default.jpg'
    ];

    mkdir('./img/'.$nik, 0755, true);

    $this->m_cuti->insertData($data,'tb_user');

    $thnSkrg = date('Y');
    $id = $this->m_cuti->editData(['user_nik' => $nik], 'tb_user')->row_array();
    $idJth = $this->m_cuti->editData(['jth_tahun' => $thnSkrg], 'tb_jatahcuti')->row_array();

    if($idJth['jth_tahun'] == $thnSkrg) {
      $this->m_cuti->insertData([
        'usr_id' => $id['user_id'], 
        'jth_id' => $idJth['jth_id'],
        'sisa_awal' => 12,
        'sisa_pemakaian' => 12], 'tb_sisacuti');

        $this->session->set_flashdata('pesan', '<script>successPegawai();</script>');
        redirect('pegawai');
    }
  }
  
  public function editPegawai($id)
  {
    $data = [
        'title' => "Edit Data Pegawai",
        'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row_array(),
      	'kar' => $this->m_cuti->editData(['user_id' => $id],'tb_user')->row_array(),
         'divisi' => $this->m_cuti->getData('tb_divisi')->result(),
        'jabatan' => $this->m_cuti->getData('tb_jabatan')->result()
      ];
  
      $this->load->view('template/header',$data);
      $this->load->view('template/sidebar',$data);
      $this->load->view('pegawai/edit',$data);
      $this->load->view('template/footer');
  }
  
  public function edit()
  {
  	$this->form_validation->set_rules('nama','Nama','required|trim',[
			'required' => 'Wajib masukan nama karyawan!'
	]);
    $this->form_validation->set_rules('nik','Nomor Induk Karyawan','required|trim',[
      'required' => 'Wajib masukan nomor induk karyawan!'
    ]);
    $this->form_validation->set_rules('jk','Jenis Kelamin Karyawan','required',[
      'required' => 'Wajib masukan jenis kelamin karyawan!'
    ]);
    $this->form_validation->set_rules('tglLahir','Tanggal lahir Karyawan','required|trim',[
      'required' => 'Wajib masukan tanggal lahir karyawan!'
    ]);
    $this->form_validation->set_rules('divisi','Divisi','required|trim',[
      'required' => 'Wajib pilih divisi karyawan!'
    ]);
    $this->form_validation->set_rules('jabatan','Jabatan','required|trim',[
      'required' => 'Wajib pilih jabatan karyawan!'
    ]);
    $this->form_validation->set_rules('tglMasuk','Tanggal Masuk Perusahaan','required|trim',[
      'required' => 'Wajib pilih jabatan karyawan!'
    ]);
    $this->form_validation->set_rules('email','Email','required|trim',[
      'required' => 'Wajib masukan email karyawan!',
    ]);
    $id = htmlspecialchars($this->input->post('id'));
    
    if($this->form_validation->run() == false) {
    	redirect('pegawai/editPegawai/'.$id);
    } else {
        $data = [
          'user_nik' => htmlspecialchars($this->input->post('nik')),
          'user_nama' => htmlspecialchars($this->input->post('nama')),
          'user_jk' => htmlspecialchars($this->input->post('jk')),
          'user_tglLahir' => htmlspecialchars($this->input->post('tglLahir')),
          'user_divisi' => htmlspecialchars($this->input->post('divisi')),
          'user_jabatan' => htmlspecialchars($this->input->post('jabatan')),
          'user_email' => htmlspecialchars($this->input->post('email')),
          'user_tglMasukPerusahaan' => htmlspecialchars($this->input->post('tglMasuk')),
        ];
    $where = ['user_id' => htmlspecialchars($this->input->post('id'))];
    $this->m_cuti->updateData($data,$where,'tb_user');
    
    redirect('pegawai');
  	}
  }
  
  public function hapusPegawai($id)
  {
  	$this->m_cuti->deleteData(['user_id' => $id], 'tb_user');
    redirect('pegawai');
  } 

  public function divisi()
  {
    $this->form_validation->set_rules('divisi', 'Divisi', 'required');
    
    
    if ($this->form_validation->run() == FALSE) {
      $data = [
        'title' => "Divisi",
        'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')], 'tb_user')->row_array(),
        'divisi' => $this->m_cuti->getData('tb_divisi')->result()
      ];
      $this->load->view('template/header',$data);
      $this->load->view('template/sidebar',$data);
      $this->load->view('pegawai/divisi',$data);
      $this->load->view('template/footer');
    } else {
      $this->inputDivisi();
    } 
  }

  private function inputDivisi()
  {
    $div = htmlspecialchars($this->input->post('divisi'));

    $data = ['divisi_nama' => $div];
    $this->m_cuti->insertData($data,'tb_divisi');

    redirect('pegawai/divisi');
  }

  public function hapusDivisi($id)
  {
    $this->m_cuti->deleteData(['divisi_id' => $id], 'tb_divisi');
    redirect('pegawai/divisi');
  }

  public function jabatan()
  {
    $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
    
    
    if ($this->form_validation->run() == FALSE) {
      $data = [
        'title' => "Jabatan",
        'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')], 'tb_user')->row_array(),
        'jabatan' => $this->m_cuti->getData('tb_jabatan')->result()
      ];
      $this->load->view('template/header',$data);
      $this->load->view('template/sidebar',$data);
      $this->load->view('pegawai/jabatan',$data);
      $this->load->view('template/footer');
    } else {
      $this->inputJabatan();
    } 
  }

  private function inputJabatan()
  {
    $jbtn = htmlspecialchars($this->input->post('jabatan'));

    $data = ['jabatan_nama' => $jbtn];
    $this->m_cuti->insertData($data,'tb_jabatan');

    redirect('pegawai/jabatan');
  }

  public function hapusJabatan($id)
  {
    $this->m_cuti->deleteData(['jabatan_id' => $id], 'tb_jabatan');
    redirect('pegawai/jabatan');
  }

  public function detailPegawai($id)
  {
    $data = [
      'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row_array(),
      'usr' => $this->m_cuti->editData(['user_id' => $id],'tb_user')->row(),
      'title' => 'Data Detail Pegawai',
      'sisa' => $this->m_cuti->editData(['usr_id' => $id],'tb_sisacuti')->row()
    ];

    $this->load->view('template/header',$data);
    $this->load->view('template/sidebar',$data);
    $this->load->view('pegawai/detail',$data);
    $this->load->view('template/footer');
  }
}