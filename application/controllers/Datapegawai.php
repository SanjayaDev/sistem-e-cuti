<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datapegawai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		protect(); 
  }

  public function index()
  {
    $data = [
      'title' => "Data Pegawai",
      'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('item')],'tb_user')->row_array()
    ];

    $this->load->view('template/header',$data);
    $this->load->view('template/sidebar',$data);
    $this->load->view('pegawai/index',$data);
    $this->load->view('template/footer');
  }
}