<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		genNoRef();
	}

	public function index()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|trim',[
			'required' => 'Wajib masukan email!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim',[
			'required' => 'Wajib masukan password!'
		]);

		if($this->form_validation->run() == false) {
			$data = [
				'title' 		=> 'Login Karyawan - PT. AMKA Indonesia',
				];
			$this->load->view('login',$data);
		} else {
			$this->cek_login();
		}	
	}

	private function cek_login()
	{
		$email = html_escape($this->input->post('email'));
		$pass = $this->input->post('password');
		$cek = $this->m_cuti->editData(['user_email' => $email],'tb_user')->row_array();


		if($cek) {
			if($cek['user_active'] == 1) {
				if(password_verify($pass, $cek['user_password'])) {
					$data = [
						'id' => $cek['user_id'],
						'role' => $cek['user_role'],
						'email' => $cek['user_email'],
						'divisi' => $cek['user_divisi']
					];
					$this->session->set_userdata($data);

					if($cek['user_role'] == 1) {
						redirect('cuti');
					} elseif($cek['user_role'] == 2) {
						redirect('pegawai');
					} else {
						redirect('user');
					}
				} else {
					$this->session->set_flashdata('pesan', '<script>gagalPass();</script>');
			redirect('login');
				}
			} else {
				$this->session->set_flashdata('pesan', '<script>gagalAktif();</script>');
			redirect('login');
			}
		} else {
			$this->session->set_flashdata('pesan', '<script>gagalEmail();</script>');
			redirect('login');
		}
	}

	public function register() 
	{
		$this->form_validation->set_rules('nama','Nama','required|trim',[
			'required' => 'Wajib masukan nama karyawan!'
		]);
		$this->form_validation->set_rules('nik','Nomor Induk Karyawan','required|trim',[
			'required' => 'Wajib masukan nomor induk karyawan!'
		]);
		$this->form_validation->set_rules('jk','Jenis Kelamin Karyawan','required',[
			'required' => 'Wajib masukan jenis kelamin karyawan!'
		]);
		$this->form_validation->set_rules('tglLahir','Tanggal lahir Karyawan','required|trim',[
			'required' => 'Wajib masukan tanggal lahir karyawan!'
		]);
		$this->form_validation->set_rules('email','Email','required|trim|is_unique[tb_user.user_email]',[
			'required' => 'Wajib masukan email karyawan!',
			'is_unique' => 'Email telah didaftarkan!'
		]);
		$this->form_validation->set_rules('pass','Password','required|trim',[
			'required' => 'Wajib masukan password baru karyawan!'
		]);
		$this->form_validation->set_rules('divisi','Divisi','required|trim',[
			'required' => 'Wajib pilih divisi karyawan!'
		]);
		$this->form_validation->set_rules('jabatan','Jabatan','required|trim',[
			'required' => 'Wajib pilih jabatan karyawan!'
		]);
		$this->form_validation->set_rules('tglMasuk','Tanggal Masuk Perusahaan','required|trim',[
			'required' => 'Wajib mengisi tanggal masuk Perusahaan!'
		]);
		$this->form_validation->set_rules('ref','Nomor Refrensi','required|trim',[
			'required' => 'Wajib masukan nomor refrensi!'
		]);
		$this->form_validation->set_rules('nik','Nomor Induk Karyawan','required|trim',[
			'required' => 'Wajib masukan nomor induk karyawan!'
		]);

		if($this->form_validation->run() == false) {
			$data = [
				'title' 		=> 'Register Karyawan - PT. AMKA Indonesia',
				'jabatan'		=> $this->m_cuti->getDataJabatan()->result(),
				'divisi'		=> $this->m_cuti->getDataDivisi()->result()
			];
			$this->load->view('register', $data);
		} else {
			$ref 			= htmlspecialchars($this->input->post('ref'));
			
			$cekRef = $this->m_cuti->editData(['no_ref' => $ref],'tb_norefrensi')->row_array();
			if(!$cekRef) {
				$this->session->set_flashdata('pesan', '<div class="alert alert-warning alert-dismissible fade show">
					<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Gagal mendaftar!</strong> Nomor Refrensi yang anda masukan salah!.
					</div>');
				redirect('register');
			} else {
				$token = rand(1, 1000000);

				$this->email->from('sanjaya@localhost', 'HR Departmen PT Amka Indonesia');
				$this->email->to($this->input->post('email'));
				
				$this->email->subject('Verifikasi Email Pendaftaran');
				$this->email->message('Terima kasih sudah mendaftarkan diri anda. Nomor verfikasi anda adalah '.$token);

				$this->email->send();

				date_default_timezone_set('Asia/Jakarta');


				$data = [
					'nik' => html_escape($this->input->post('nik')),
					'nama' => html_escape($this->input->post('nama')),
					'jk' => html_escape($this->input->post('jk')),
					'tglLahir' => html_escape($this->input->post('tglLahir')),
					'divisi' => html_escape($this->input->post('divisi')),
					'jabatan' => html_escape($this->input->post('jabatan')),
					'email' => html_escape($this->input->post('email')),
					'tglMasuk' => html_escape($this->input->post('tglMasuk')),
					'pass' => password_hash($this->input->post('pass'), PASSWORD_DEFAULT),
					'role' => $cekRef['no_role'],
					'active' => 1,
					'date' => time(),
					'status' => "Verifikasi",
					'token' => $token
				];

				$this->session->set_userdata($data);
				redirect('verifikasi');
			}		
		}
	}

	public function verify()
	{
		if($this->session->userdata('status') != "Verifikasi") {
			$this->session->set_flashdata('pesan', '<script>failed();</script>');
			redirect('register');
		} else {
			$this->form_validation->set_rules('token', 'Nomor Verifikasi', 'required|trim|min_length[4]', [
				'required' => 'Masukan Nomor Verifikasi terlebih dahulu!',
				'min_length' => 'Masukan minimal 5 karakter numerik!'
			]);
			if($this->form_validation->run() == false) {
				$data['title'] = 'Verfikasi Email';
				$this->load->view('verif', $data);

			} elseif($this->session->userdata('token') != $this->input->post('token')) { 
				$this->session->set_flashdata('pesan', '<script>failed();</script>');
				redirect('verifikasi');

			} else {
				$data = [
					'user_nik' => $this->session->userdata('nik'),
					'user_nama' => $this->session->userdata('nama'),
					'user_jk' => $this->session->userdata('jk'),
					'user_tglLahir' => $this->session->userdata('tglLahir'),
					'user_email' => $this->session->userdata('email'),
					'user_password' => $this->session->userdata('pass'),
					'user_divisi' => $this->session->userdata('divisi'),
					'user_jabatan' => $this->session->userdata('jabatan'),
					'user_tglMasukPerusahaan' => $this->session->userdata('tglMasuk'),
					'user_role' => $this->session->userdata('role'),
					'user_active' => 1,
					'user_dateCreated' => $this->session->userdata('date'),
					'user_img' => 'default.jpg'
				];

				mkdir('./img/'.$this->session->userdata('nik'), 0755, true);

				$this->m_cuti->insertData($data,'tb_user');
				
				$thnSkrg = date('Y');
				$id = $this->m_cuti->editData(['user_nik' => $this->session->userdata('nik')], 'tb_user')->row_array();
				$idJth = $this->m_cuti->editData(['jth_tahun' => $thnSkrg], 'tb_jatahcuti')->row_array();

				if($idJth['jth_tahun'] == $thnSkrg) {
					$this->m_cuti->insertData([
						'usr_id' => $id['user_id'], 
						'jth_id' => $idJth['jth_id'],
						'sisa_awal' => 12,
						'sisa_pemakaian' => 12], 'tb_sisacuti');

						$this->session->set_flashdata('pesan', '<script>sukses();</script>');
						redirect('login');
				}
			}
		}
	}

	public function blocked()
	{
		$data['title'] = "Halaman tidak ditemukan";
		$this->load->view('block', $data);
	}
}