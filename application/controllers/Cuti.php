<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuti extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		protect();
  }

  public function index()
  {
    $this->form_validation->set_rules('nik', 'NIK Karyawan', 'trim|required|min_length[7]|max_length[12]');
    $this->form_validation->set_rules('hcuti', 'Hari Cuti', 'required');
    $this->form_validation->set_rules('tglMulai', 'Tanggal Mulai Cuti', 'required');
    $this->form_validation->set_rules('tglSelesai', 'Tanggal Selesai Cuti', 'required');
    $this->form_validation->set_rules('tglKerja', 'Tanggal Mulai Kerja', 'required');
    $this->form_validation->set_rules('alasan', 'Alasan Cuti', 'required');
    
    if ($this->form_validation->run() ==  FALSE) {
      $data = [
        'title' => "Data Cuti",
        'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row_array(),
        'divisi' => $this->m_cuti->getData('tb_divisi')->result()
      ];
      
      $this->load->view('template/header',$data);
      $this->load->view('template/sidebar',$data);
      $this->load->view('cuti/index',$data);
      $this->load->view('template/footer');
    } else {
      $nik = htmlspecialchars($this->input->post('nik'));
      $hcuti = htmlspecialchars($this->input->post('hcuti'));
      $sql = $this->m_cuti->editData(['user_nik' => $nik], 'tb_user')->row_array();

      if($sql) {
        $sql = $this->m_cuti->editData(['usr_id' => $sql['user_id']], 'tb_sisacuti')->row_array();
        if($sql['sisa_pemakaian'] < 1) {
          $this->session->set_flashdata('pesan', '<script>cutiJatah();</script>');
        
          redirect('cuti');
        } else {
          $sisa = $sql['sisa_pemakaian'] - $hcuti;
          if($sisa < 1) {
            $this->session->set_flashdata('pesan', '<script>cutiHabis();</script>');
        
            redirect('cuti');
          } else {
            $this->inputCuti();
          }
        }
      } else {
        $this->session->set_flashdata('pesan', '<script>cutiNIK()</script>');
        
        redirect('cuti');
      }
    }
  }

  private function inputCuti()
  {
    $nik        = htmlspecialchars($this->input->post('nik'));
    $hcuti      = htmlspecialchars($this->input->post('hcuti'));
    $tglMulai   = htmlspecialchars($this->input->post('tglMulai'));
    $tglSelesai = htmlspecialchars($this->input->post('tglSelesai'));
    $tglKerja   = htmlspecialchars($this->input->post('tglKerja'));
    $alasan     = htmlspecialchars($this->input->post('alasan'));

    $usr = $this->m_cuti->editData(['user_nik' => $nik], 'tb_user')->row();
    $sisa = $this->m_cuti->editData(['usr_id' => $usr->user_id], 'tb_sisacuti')->row();
    $sPemakaian = $sisa->sisa_pemakaian;

    $sAkhirPemakaian = $sPemakaian - $hcuti;
    $thn = date('Y');
    $thnIni = $this->m_cuti->editData(['jth_tahun' => $thn],'tb_jatahcuti')->row();

    $where = ['usr_id' => $usr->user_id, 'jth_id' => $thnIni->jth_id];
    $data = [
      'sisa_awal' => $sPemakaian,
      'sisa_pemakaian' => $sAkhirPemakaian
    ];
    $this->m_cuti->updateData($data,$where,'tb_sisacuti');
    $sisa = $this->m_cuti->editData(['usr_id' => $usr->user_id], 'tb_sisacuti')->row();

    $data = [
      'user_id' => $usr->user_id,
      'sisa_id' => $sisa->sisa_id,
      'cuti_hari' => $hcuti,
      'cuti_awal' => $sisa->sisa_awal,
      'cuti_dariTgl' => $tglMulai,
      'cuti_sampaiTgl' => $tglSelesai,
      'cuti_keperluan' => $alasan,
      'cuti_sisa' => $sisa->sisa_pemakaian,
      'cuti_divisi' => $usr->user_divisi,
      'cuti_tglMasukKerja' => $tglKerja,
      'cuti_statusApprov' => 0
    ];

    $this->m_cuti->insertData($data,'tb_datacuti');

    $this->session->set_flashdata('pesan', '<script>cutiSukses();</script>');
    
    redirect('cuti');
  }

  public function detailCuti($id)
  {
    $cuti = $this->m_cuti->editData(['cuti_id' => $id], 'tb_datacuti')->row();
    $user = $cuti->user_id;
    $data = [
      'title' => 'Detail Cuti',
      'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row_array(),
      'cuti' => $this->db->query("SELECT * FROM tb_datacuti,tb_user WHERE tb_datacuti.user_id='$user' AND tb_user.user_id='$user'")->result(),
      'usr' => $this->m_cuti->editData(['user_id' => $user],'tb_user')->row(),
      'sisa' => $this->m_cuti->editData(['sisa_id' => $cuti->sisa_id],'tb_sisacuti')->row()
    ];

    $this->load->view('template/header',$data);
    $this->load->view('template/sidebar',$data);
    $this->load->view('cuti/detail',$data);
    $this->load->view('template/footer');
  }

  public function approv($id)
  {
    $data = ['cuti_statusApprov' => 1];
    $where = ['cuti_id' => $id];

    $this->m_cuti->updateData($data,$where,'tb_datacuti');
    $this->session->set_flashdata('pesan', '<script>approvCuti();</script>');
    
    redirect('cuti');
  }

  public function reject($id)
  {
    $data = ['cuti_statusApprov' => 2];
    $where = ['cuti_id' => $id];

    $this->m_cuti->updateData($data,$where,'tb_datacuti');
    $this->session->set_flashdata('pesan', '<script>rejectCuti();</script>');

    $data = $this->m_cuti->editData($where,'tb_datacuti')->row();
    $sisa = $this->m_cuti->editData(['sisa_id' => $data->sisa_id],'tb_sisacuti')->row();
    
    $data1 = [
      'sisa_awal' => $sisa->sisa_awal - $data->cuti_hari,
      'sisa_pemakaian' => $sisa->sisa_pemakaian + $data->cuti_hari
    ];
    $where1 = ['sisa_id' => $data->sisa_id];

    $this->m_cuti->updateData($data1,$where1,'tb_sisacuti');
    
    $this->session->set_flashdata('pesan', '<script>rejectCuti();</script>');
    redirect('cuti');
  }

  public function sisaCuti()
  {
    $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required');
    $this->form_validation->set_rules('jatah', 'Jatah', 'trim|required');

    if ($this->form_validation->run() == FALSE) {
      $data = [
        'title' => 'Data Sisa Cuti',
        'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row_array(),
        'sisa' => $this->m_cuti->joinSisa()->result(),
        'tahun' => $this->m_cuti->getData('tb_jatahcuti')->result()
      ];
      
      $this->load->view('template/header',$data);
      $this->load->view('template/sidebar',$data);
      $this->load->view('cuti/sisa',$data);
      $this->load->view('template/footer');
    } else {
      $this->inputJatah();
    }
  }

  private function inputJatah()
  {
    $tahun = htmlspecialchars($this->input->post('tahun'));
    $jatah = htmlspecialchars($this->input->post('jatah'));

    $data = [
      'jth_tahun' => $tahun,
      'jth_hari' => $jatah
    ];

    $this->m_cuti->insertData($data,'tb_jatahcuti');
    $data1 = $this->m_cuti->editData(['jth_tahun' => $tahun],'tb_jatahcuti')->row();
    $user = $this->m_cuti->getData('tb_user')->result();

    foreach($user as $s) {
      $data2 = [
        'usr_id' => $s->user_id,
        'jth_id' => $data1->jth_id,
        'sisa_awal' => $data1->jth_hari,
        'sisa_pemakaian' => $data1->jth_hari
      ];

      $this->m_cuti->insertData($data2,'tb_sisacuti');
    }

    redirect('cuti/sisaCuti');
  }

  public function hapusJatah($id)
  {
    $user = $this->m_cuti->getData('tb_user')->result();
    $jth = $this->m_cuti->editData(['jth_id' => $id],'tb_jatahcuti');

    foreach($user as $u) {
      $where = ['usr_id' => $u->user_id, 'jth_id' => $jth->jth_id];
      $this->m_cuti->deleteData($where,'tb_sisacuti');
    }

    $where = ['jth_id' => $id];
    $this->m_cuti->deleteData($where,'tb_jatahcuti');

    redirect('cuti/sisaCuti');
  }
}