<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
  public function __construct()
	{
		parent::__construct();
		protect();
  }

  public function index()
  {
    $data['title'] = "My Profile";
    $data['user'] = $this->m_cuti->editData(['user_id' => $this->session->userdata('id')], 'tb_user')->row_array();
    
    $this->load->view('template/header',$data);
    $this->load->view('template/sidebar',$data);
    $this->load->view('user/index',$data);
    $this->load->view('template/footer');
  }

  public function editProfile()
  {
    $this->form_validation->set_rules('nama', 'Nama Karyawan', 'required',[
      'required' => 'Nama Karyawan wajib di isi!'
    ]);
    $this->form_validation->set_rules('jk', 'Jenis Kelamin Karyawan', 'required',[
      'required' => 'Jenis Kelamin wajib di isi!'
    ]);
    $this->form_validation->set_rules('tglLahir', 'Tanggal Lahir Karyawan', 'required',[
      'required' => 'Tanggal Lahir wajib di isi!'
    ]);
    $this->form_validation->set_rules('email', 'Email Karyawan', 'required|trim|valid_email',[
      'required' => 'Email Karyawan wajib di isi!',
      'valid_email' => 'Masukan format email dengan benar!'
    ]);

    if($this->form_validation->run() != TRUE) {
      $data['title'] = "Edit Profile";
      $data['user'] = $this->m_cuti->editData(['user_id' => $this->session->userdata('id')], 'tb_user')->row_array();
      
      $this->load->view('template/header',$data);
      $this->load->view('template/sidebar',$data);
      $this->load->view('user/editProfile',$data);
      $this->load->view('template/footer');
    } else {
      $this->updateProfile();
    }
  }

  private function updateProfile()
  {
    $id       = htmlspecialchars($this->input->post('id'));
    $nama     = htmlspecialchars($this->input->post('nama'));
    $jk       = htmlspecialchars($this->input->post('jk'));
    $tglLahir = htmlspecialchars($this->input->post('tglLahir'));
    $email    = htmlspecialchars($this->input->post('email'));
    $gambar   = $_FILES['gbr']['name'];
    
    //var_dump($gambar);


    if($gambar != '') {
      $where = ['user_id' => $this->session->userdata('id')];
      $data = $this->m_cuti->editData($where,'tb_user')->row();
  
      $img = $data->user_img;
      $nik = $data->user_nik;
      $file = file_exists('./img/'.$nik."/".$img);
      
      //var_dump($file);
      
      if($file) {
        unlink('./img/'.$nik."/".$img);
      }

      $config['upload_path']          = './img/'.$nik.'/';
      $config['allowed_types']        = 'jpeg|jpg|png';
      $config['max_size']             = 2000;
      $config['max_width']            = 1024;
      $config['max_height']           = 1024;


       $this->load->library('upload');
       $this->upload->initialize($config);
            
      
      if(!$this->upload->do_upload('gbr')) {
        //print_r($this->upload->display_errors());
        $error = $this->session->set_flashdata('pesan', '<script>alert("Gagal upload gambar!");</script>');
        redirect(base_url('user/editProfile', $error));
      } else {
        $gambar = $this->upload->data('file_name');
      }
    } 
    
    $data = [
      'user_nama' => $nama,
      'user_email' => $email,
      'user_img' => $gambar,
      'user_jk' => $jk,
      'user_tglLahir' => $tglLahir
    ];
    $where = ['user_id' => $id];

    $this->m_cuti->updateData($data,$where,'tb_user');

    $this->session->set_flashdata('pesan','<script>ubahProfile();</script>');
    redirect(base_url('user'));
  }

  public function changePassword()
  {
    $this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required',[
      'required' => 'Wajib masukan password lama anda!'
    ]);
    $this->form_validation->set_rules('passBaru', 'Password Lama', 'trim|required|matches[passBaru1]',[
      'required' => 'Wajib masukan password baru anda!',
      'matches' => 'Password anda tidak sesuai!'
    ]);
    $this->form_validation->set_rules('passBaru1', 'Password Lama', 'trim|required|matches[passBaru]',[
      'required' => 'Wajib masukan password baru anda!',
      'matches' => 'Password anda tidak sesuai!'
    ]);

    
    if ($this->form_validation->run() == FALSE) {
      $data = [
        'title' => "Ubah Password",
        'user' => $this->m_cuti->editData(['user_id' => $this->session->userdata('id')], 'tb_user')->row_array()
      ];
      $this->load->view('template/header',$data);
      $this->load->view('template/sidebar',$data);
      $this->load->view('user/changePassword',$data);
      $this->load->view('template/footer'); 
    } else {
      $passLama = $this->input->post('passLama');
      $passBaru = password_hash($this->input->post('passLama'), PASSWORD_DEFAULT);
      $usr = $this->m_cuti->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row();

      if(password_verify($passLama, $usr->user_password)) {
        $where = ['user_id' => $usr->user_id];
        $data = ['user_password' => $passBaru];

        $this->m_cuti->updateData($data,$where,'tb_user');
        $this->session->set_flashdata('pesan', '<script>successPassword();</script>');
        
        redirect('user/changePassword');
      } else {
        $this->session->set_flashdata('pesan', '<script>wrongPassword();</script>');
        
        redirect('user/changePassword');
      }
    }
  }
}