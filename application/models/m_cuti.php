<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_cuti extends CI_Model {
  public function getDataJabatan()
  {
    return $this->db->get('tb_jabatan');
  }

  public function getDataDivisi()
  {
    return $this->db->get('tb_divisi');
  }

  public function editData($where,$table)
  {
    return $this->db->get_where($table,$where);
  }

  public function insertData($data,$table)
  {
    $this->db->insert($table,$data);
  }

  public function getData($table)
  {
    return $this->db->get($table);
  }

  public function updateData($data,$where,$table)
  {
    $this->db->where($where)->update($table,$data);
  }

  public function deleteData($where,$table)
  {
    $this->db->where($where)->delete($table);
  }


  public function joinSisa()
  {
    return $this->db->select('*')
                    ->from('tb_sisacuti')
                    ->join('tb_user', 'tb_user.user_id=tb_sisacuti.usr_id')
                    ->join('tb_jatahcuti', 'tb_sisacuti.jth_id=tb_jatahcuti.jth_id')
                    ->get();
  }
}