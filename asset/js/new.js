function sukses() {
  swal({
      title: "Sukses Registrasi Karyawan",
      text: "Sukses registrasi karyawan! Silahkan Login!",
      icon: "success",
      button: "Tutup",
    });
} function gagalEmail() {
  swal({
      title: "Gagal Login",
      text: "Email yang anda masukan tidak terdaftar!",
      icon: "error",
      button: "Tutup",
    });
} function gagalAktif() {
  swal({
      title: "Gagal Login",
      text: "Akun anda belum diaktifkan!",
      icon: "error",
      button: "Tutup",
    });
} function gagalPass() {
  swal({
      title: "Gagal Login",
      text: "Password anda salah!",
      icon: "error",
      button: "Tutup",
    });
} function blocked() {
  swal({
      title: "Gagal Masuk",
      text: "Anda harus login dahulu!",
      icon: "error",
      button: "Tutup",
    });
} function uploadGbr() {
  swal({
      text: "Gagal upload gambar!",
      icon: "error",
      button: "Tutup",
    });
} function noRef() {
  swal({
      title: "Gagal Menambahkan pegawai!",
      text: "Nomor refrensi anda salah!",
      icon: "error",
      button: "Tutup",
    });
} function successPegawai() {
  swal({
      title: "Sukses Menambahkan pegawai!",
      text: "Sukses menambahkan pegawai baru!",
      icon: "success",
      button: "Tutup",
    });
} function ubahProfile() {
  swal({
      title: "Sukses Mengupdate Profile!",
      text: "Data anda berhasil di update!",
      icon: "success",
      button: "Tutup",
    });
} function cutiNIK() {
  swal({
      title: "NIK tidak ditemukan!",
      text: "NIK yang anda masukan tidak ada dalam database!",
      icon: "warning",
      button: "Tutup",
    });
} function cutiJatah() {
  swal({
      title: "Jatah cuti sudah habis!",
      text: "Jatah cuti karyawan tersebut sudah habis!",
      icon: "info",
      button: "Tutup",
    });
} function cutiHabis() {
  swal({
      title: "Jatah cuti tidak cukup!",
      text: "Jatah cuti karyawan tersebut tidak mencukupi!",
      icon: "info",
      button: "Tutup",
    });
} function cutiSukses() {
  swal({
      title: "Menambahkan data cuti berhasil!",
      text: "Data cuti telah tersimpan kedalam database!",
      icon: "success",
      button: "Tutup",
    });
} function approvCuti() {
  swal({
      title: "Approved berhasil!",
      text: "Anda telah mengizinkan cuti!",
      icon: "success",
      button: "Tutup",
    });
} function rejectCuti() {
  swal({
      title: "Reject berhasil!",
      text: "Anda tidak mengizinkan cuti!",
      icon: "info",
      button: "Tutup",
    });
}